#ifndef __CNAND_H_
#define __CNAND_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <memory.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>

#ifdef WIN32
//#include <Dbgeng.h>
#else // !WIN32
#include "FreeRTOSInclude.h"
#define CNAND_MLOCK

#endif

	//#define IDBLOCK	((uint64_t)(0x4578A3E3F36C))
#define IDBLOCK	((uint64_t)(0xF8BFBCCCDDDDEEEF))

#ifndef WIN32
#include "tlsf.h"
#define cnand_calloc(x,y) calloc(x,y)
#else
#define cnand_calloc(x,y) calloc(x,y)
#endif

#ifdef CNAND_MLOCK
#define CNAND_MLOCK_T            	MutexHandle_t
#define CNAND_CREATE_LOCK(Mutex)    vMutexCreate(Mutex)
#define CNAND_DESTROY_LOCK(Mutex)    vMutexDelete(Mutex)
#define CNAND_ACQUIRE_LOCK(Mutex)    xMutexTake(Mutex)
#define CNAND_RELEASE_LOCK(Mutex)    xMutexGive(Mutex)
#else
#define CNAND_CREATE_LOCK(Mutex)
#define CNAND_DESTROY_LOCK(Mutex)
#define CNAND_ACQUIRE_LOCK(Mutex)
#define CNAND_RELEASE_LOCK(Mutex)
#endif

	typedef struct
	{
			uint64_t idblock;						//���������� ������������� �����, ������� � ��� ��� ���� ������������ � �������
			uint32_t createcounter;					//������� ������, �������� ��������� ����� ������� ����
			uint32_t overwrites;					//����� ����������� �����
	} cnand_title;

	struct cnand_param
	{
			uint32_t startblock;	//��������� ����
			uint32_t endblock;		//��������� ����
			uint32_t reserveblock;  //���������� ��������� ������
			uint32_t pageperblock;	//������� � �����
			uint32_t pagesize;		//������ ��������
			uint32_t clustersize;	//������ ��������
			uint32_t pagesparesize; //�������������� ����� � ��������
			//����������� ��������
			uint32_t fullpagesize;  //������ ������ ��������
			uint32_t numberblockfortable; //���������� ������ ��� �������
	};

	typedef struct
	{
			uint32_t createcounter;		//������� ������
			uint32_t createcounteradd;	//���������� � ��������
			uint32_t currentblock;		//������� ����, ����������

	} cnand_searchsystemblock;

	struct cnand_driver
	{
			uint32_t (*drv_write_page_fn)(const uint16_t nand_block, const uint8_t page, const uint8_t *data);
			uint32_t (*drv_read_page_fn)(const uint16_t nand_block, const uint8_t page, uint8_t *data);
			uint32_t (*drv_erase_fn)(const uint16_t nand_block);
	};

	typedef enum
	{
		CNAND_STATE_OK = 0x00, //�������� ��������� ������
		CNAND_STATE_ERROR_NEWBLOCK, //������ ������ ���������� �����
		CNAND_STATE_ERROR_BADBLOCK, //���� ���� ��� ������/������
		CNAND_STATE_ERROR_UNKOWN, //����������� ������, ��� ��������� ������� �� ���� ���� ����
		CNAND_STATE_ERROR_NOTALLOCATIONBLOCK, //��� ������ � �������
		CNAND_STATE_ERROR_NOINIT, //������� �� ����������
		CNAND_STATE_ERROR_READ,
	} cnand_state;

	typedef enum
	{
		CNAND_BLOCK_STATE_UNKNOWN = 0,			//������ ����� �� ��������
		CNAND_BLOCK_STATE_SCANNING,				//���� ����������, ��� ������ ������������ ����������� ��� �������� ����� ������
		CNAND_BLOCK_STATE_FREE,					//��������� ����, ����� ���� �������
		CNAND_BLOCK_STATE_EMPTY,				//���� �� �������� ������, � ������ �������� �� ������
		CNAND_BLOCK_STATE_BADSYSTEM,			//�������� ������� ���������� �� �����, �������� �� ������
		CNAND_BLOCK_STATE_FULL,					//���� ����� �������, ������� ������������
		CNAND_BLOCK_STATE_DEAD = 1 << 15,		//���� ����
	} cnand_block_state;

	typedef enum
	{
		CNAND_DEV_STATE_NOINIT = 0,	//������� �� ������������������
		CNAND_DEV_STATE_EMPTY,		//������� ������, (��� ������� ������)
		CNAND_DEV_STATE_READY,		//������� ������ � �������������

	} cnand_dev_state;

	typedef enum
	{
		CNAND_BLOCKINCARCHE_STATE_NOCARCHE = 0x00,
		CNAND_BLOCINCKARCHE_STATE_READCARCHE,
		CNAND_BLOCINCKARCHE_STATE_WRITECARCHE,

	} cnand_blockincarche_state;

#define CNAND_ERROR_BLOCKNUM (0x7FFF)

	typedef struct //��������� ��� �������� ���������� �� ������ �������� ����� � ��� ����, ����� ������������� ��������������
	{
			uint8_t* cacheblock;	//��� �����
			uint16_t currentblock;	//������������� ����
			uint32_t sectorinblock;			//�������� � �����
			uint32_t sectorforsystemtable; //�������� ��� ��������� �������
			cnand_blockincarche_state blockincarchestate; //���� ��������� ��� ���������
	} cnand_cacheblock;

	typedef struct
	{
			uint32_t notempty; //���� �� ������ �����

	} cnand_search;

	typedef struct
	{
			struct cnand_param param;
			struct cnand_driver drv;

			uint32_t createcounter;					//����� ������ �����
			cnand_block_state* statusblock;			//������� ������� �����, ����������
			uint16_t* blocktable;					//������� ������������ ������ ����������� ����������,������ ��������� �� ��������� ����
			uint32_t* overwrites;					//������� ������ ���������� ������, ����������
			cnand_cacheblock cacheblock;			//��� �����
			cnand_search searchblock;				//��������� ������ �����
			cnand_dev_state devstate;				//������ ������������� �������
#ifdef CNAND_MLOCK
			CNAND_MLOCK_T cnandmutex;				//�������� �������������
#endif

	} cnand_dev;

	int init_crutch_nand(cnand_dev* dev);
	cnand_state crutch_nand_read(cnand_dev* dev, uint8_t* buff, uint64_t sector, uint32_t count);
	cnand_state crutch_nand_write(cnand_dev* dev, const uint8_t* buff, uint64_t sector, uint32_t count);
	cnand_state crutch_sync(cnand_dev* dev);

#define  crutch_getsectorcountinblock(dev) (dev->cacheblock.sectorinblock)
#define  crutch_getsectorcount(dev) (crutch_getsectorcountinblock(dev)*((dev->param.endblock - dev->param.startblock) - dev->param.reserveblock))

	enum
	{
		CNAND_TRACE_INIT = 1 << 0,
		CNAND_TRACE_READ = 1 << 1,
		CNAND_TRACE_WRITE = 1 << 2,
		CNAND_TRACE_SYNC = 1 << 3,
		CNAND_TRACE_INIT_EMPTYBLOCK = 1 << 4,
	};

#ifdef CNAND_TARCE
#ifdef WIN32
	void dprintf(char * format, ...);
#endif
	extern int cnand_trace_mask;

#ifdef WIN32
#define  cnand_trace(msk, fmt, ...) do { \
				if (cnand_trace_mask & (msk)) \
				dprintf("cnand: " fmt /*"\n"*/, ##__VA_ARGS__); \
						} while (0)
#else
#define  cnand_trace(msk, fmt, ...) do { \
				if (cnand_trace_mask & (msk)) \
				Debug_printf("cnand: " fmt /*"\n"*/, ##__VA_ARGS__); \
						} while (0)

#endif
#else
#define cnand_trace(msk, fmt, ...)
#endif

#ifdef __cplusplus
}
#endif
#endif
