#include "emulation_nandflash.h"
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <io.h>

#define FCLOSE(X) fclose(X); \
				  X = fopen(FileName, "ab+");

static char FileName[1024];
//FILE *fp;
#define NANDFLASH_FULLPAGESIZE ((NANDFLASH_RW_PAGE_SIZE) + (NANDFLASH_SPARE_SIZE))

void Init_sdram_nand()
{
	char* TempFilePath = NULL;
	char* FilePath = NULL;
	char* Disk = NULL;

	TempFilePath = calloc(sizeof(char), 1024);
	FilePath = calloc(sizeof(char), 1024);
	Disk = calloc(sizeof(char),5);
	//HMODULE hModule = GetModuleHandle(NULL);
	memset(FilePath, 0, sizeof(FilePath));
	GetModuleFileNameA(NULL, FilePath, 1024);
	_splitpath(FilePath, Disk, TempFilePath, NULL, NULL);
	sprintf(FileName, "%s%s%s", Disk, TempFilePath, NAND_FILE_NAME);
	if (_access(FileName, 0) != 0) //��� �����
	{
		//��������
		FILE *fp = fopen(FileName, "wb+");
		if (fp == NULL)while (1);
		fseek(fp, (NANDFLASH_NUMOF_BLOCK*NANDFLASH_PAGE_PER_BLOCK*NANDFLASH_FULLPAGESIZE)+1, SEEK_SET);
		fwrite("\0", sizeof(char), sizeof("\0"), fp);
		fclose(fp);
	}
	else
	{
		FILE *fp = fopen(FileName, "r+b");
		if (fp == NULL)while (1);
		fclose(fp);
	}

	free(TempFilePath);
	free(FilePath);
	free(Disk);
}

uint32_t NandFlash_ReadPage(const uint16_t blockNum, const uint8_t PageNum, uint8_t* Buff)
{
	FILE *fp = fopen(FileName, "r+b");
	uint32_t Status;
	if (fp == NULL) return false;
	if (fseek(fp, (blockNum*NANDFLASH_PAGE_PER_BLOCK*NANDFLASH_FULLPAGESIZE) + (PageNum*NANDFLASH_FULLPAGESIZE), SEEK_SET) == 0)
	{
		if (fread(Buff, NANDFLASH_FULLPAGESIZE, 1, fp) > 0)
		{
			//fclose(fp);
			fclose(fp);
			Status = true;
			return Status;
		}
		fclose(fp);
		return false;
	}
	fclose(fp);
	return false;
}

uint32_t NandFlash_PageProgram(const uint16_t blockNum, const uint8_t PageNum, const uint8_t* Buff)
{
	uint32_t Status;
	long Seek = (blockNum*NANDFLASH_PAGE_PER_BLOCK*NANDFLASH_FULLPAGESIZE) + (PageNum*NANDFLASH_FULLPAGESIZE);
	FILE *fp = fopen(FileName, "r+b");
	if (fp == NULL) return false;

	if (fseek(fp, Seek, SEEK_SET) == 0)
	{
		if (fwrite(Buff, NANDFLASH_FULLPAGESIZE, 1, fp) > 0)
		{
			fclose(fp);
			Status = true;
			return Status;
		}
		fclose(fp);
		return false;
	}
	fclose(fp);
	return false;
}

uint32_t NandFlash_BlockErase(const uint16_t blockNum)
{
	static char buff[NANDFLASH_PAGE_PER_BLOCK*NANDFLASH_FULLPAGESIZE];
	FILE *fp = fopen(FileName, "r+b");
	if (fp == NULL) return false;
	if (fseek(fp, (blockNum*NANDFLASH_PAGE_PER_BLOCK*NANDFLASH_FULLPAGESIZE), SEEK_SET) == 0)
	{
		memset(buff, 0x00, NANDFLASH_PAGE_PER_BLOCK*NANDFLASH_FULLPAGESIZE);
		if (fwrite(buff, NANDFLASH_PAGE_PER_BLOCK*NANDFLASH_FULLPAGESIZE, 1, fp) > 0)
		{
			fclose(fp);
			
			return true;
		}
		fclose(fp);
		return false;
	}
	return false;
}