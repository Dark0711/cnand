#ifndef __LPC177X_8X_CRC_H_
#define __LPC177X_8X_CRC_H_
#include <stdint.h>

typedef enum
{
	CRC_POLY_CRCCCITT = 0,			/** CRC CCITT polynomial */
	CRC_POLY_CRC16,					/** CRC-16 polynomial */
	CRC_POLY_CRC32					/** CRC-32 polynomial */
}CRC_Type;

typedef enum
{
	CRC_WR_8BIT = 1,				/** 8-bit write: 1-cycle operation */
	CRC_WR_16BIT = 2,					/** 16-bit write: 2-cycle operation */
	CRC_WR_32BIT = 4,					/** 32-bit write: 4-cycle operation */
}CRC_WR_SIZE;

void CRC_Init(CRC_Type CRCType);
void CRC_Reset(void);
uint32_t CRC_CalcBlockChecksum(void *blockdata, uint32_t blocksize, CRC_WR_SIZE SizeType);

#endif