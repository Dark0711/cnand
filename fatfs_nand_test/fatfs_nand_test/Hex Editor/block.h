
#include "stddefs.h"


struct cnand_title
{
	uint64 idblock;
	uint64 createcounter;
	uint64 overwrites;
};

public struct Cnand
{
	cnand_title Title;
	uint16 BlockTable[1024];
};


struct DataPage
{
	uint8 DataPage[2048];
	uint8 __R[64];
};

union DataPageUnion
{
	DataPage Page;
	Cnand TitleInfo;
};

struct DataBlock
{
	DataPageUnion Pages[64];
};

public struct Nanad_Flash
{
	DataBlock Blocks[1024];
};


struct DataCluster
{
	uint8 ClusterData[2048];
};

struct _AllCluster
{
	DataCluster SustemCluster[2];
	DataCluster Cluster[64];
};

public struct Nanad_Flashcl
{
	_AllCluster AllCluster[1024];
};
