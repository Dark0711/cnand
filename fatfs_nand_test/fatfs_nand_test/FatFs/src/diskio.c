/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2014        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "diskio.h"		/* FatFs lower layer API */
#include "emulation_nandflash.h"
#include "emulation_lpc177x_8x_crc.h"
#include "cnand.h"
#include <time.h>
#include "TimeSync.h"
/* Definitions of physical drive number for each drive */
#define NAND		0	/* Example: Map ATA harddisk to physical drive 0 */

#ifdef CNAND_TARCE
int cnand_trace_mask = CNAND_TRACE_INIT | CNAND_TRACE_READ | CNAND_TRACE_WRITE | CNAND_TRACE_SYNC;
#endif
cnand_dev* dev = NULL;

static DRESULT NAND_ioctl();
/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status(
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
	)
{
	return 0;
}



/*-----------------------------------------------------------------------*/
/* Initialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize(
	BYTE pdrv				/* Physical drive number to identify the drive */
	)
{

	if (pdrv == NAND)
	{		
		if (dev == NULL)
		{
			Init_sdram_nand();
			dev = calloc(sizeof(cnand_dev),1);
			if (dev == NULL) return STA_NOINIT;
			memset(dev, 0, sizeof(cnand_dev));
			//dev->drv.drv_crc_fn = CRC_CalcBlockChecksum;
			dev->drv.drv_erase_fn = NandFlash_BlockErase;
			dev->drv.drv_read_page_fn = NandFlash_ReadPage;
			dev->drv.drv_write_page_fn = NandFlash_PageProgram;
			//dev->drv.drv_gettime = CT_GetDataTime;

			dev->param.startblock = 0; 
			dev->param.reserveblock = 1;
			dev->param.endblock = NANDFLASH_NUMOF_BLOCK;
			dev->param.pagesize = NANDFLASH_RW_PAGE_SIZE;
			dev->param.pagesparesize = NANDFLASH_SPARE_SIZE;
			dev->param.pageperblock = NANDFLASH_PAGE_PER_BLOCK;
			dev->param.clustersize = NANDFLASH_RW_PAGE_SIZE;
			init_crutch_nand(dev);
		}
		return 0;
	}

	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read(
	BYTE pdrv,		/* Physical drive number to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	/* Sector address in LBA */
	UINT count		/* Number of sectors to read */
	)
{
	cnand_state state;
	if (pdrv == NAND)
	{
		state = crutch_nand_read(dev, buff, sector, count);
		if (state == CNAND_STATE_OK)
		{
			return RES_OK;
		}
	}


	return RES_PARERR;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _USE_WRITE
DRESULT disk_write(
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Sector address in LBA */
	UINT count			/* Number of sectors to write */
	)
{
	cnand_state state;
	if (pdrv == NAND)
	{
		state = crutch_nand_write(dev, buff, sector, count);
		if (state == CNAND_STATE_OK)
		{
			return RES_OK;
		}
	}


	return RES_PARERR;
}
#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL
DRESULT disk_ioctl(
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
	)
{
	//DRESULT res;
	int result;

	if (pdrv == NAND)
	{
		return NAND_ioctl(cmd, buff);
	}

	return RES_PARERR;
}
#endif

int get_fattime()
{
	struct tm * Time;

	return 0;
}

static DRESULT NAND_ioctl(BYTE cmd, void* buff)
{
	switch (cmd) {
	case CTRL_SYNC:
	{
		//������� ����
		crutch_sync(dev);
		return RES_OK;
	}
	case GET_SECTOR_COUNT:	/* Get number of user sectors */
	{
		*(DWORD*)buff = (DWORD)(crutch_getsectorcount(dev));
		return RES_OK;
	}
	case GET_BLOCK_SIZE:	/* Get erase block size */
	{
		*(DWORD*)buff = 1;//(DWORD)(crutch_getsectorcountinblock(dev));
		return RES_OK;
	}
	case GET_SECTOR_SIZE:
	{
		*(DWORD*)buff = (DWORD)(dev->param.clustersize);
		return RES_OK;
	}
	case CTRL_TRIM:			/* Erase (force blanked) a sector group */
	{

		return RES_OK;
	}
	}
	return RES_PARERR;
}


